package com.dangerouskitchen.infinite.items;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;

import com.dangerouskitchen.infinite.db.DatabaseHandler.ListTable;

/* 
Infinite Checklist - A nice and simple Android app
Copyright (C) 2014 Casey Watson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

public class ChecklistItem{

	private int group;
	private int link;
	private String name;
	private boolean checked = false;
	
	public ChecklistItem(String name, int link, int group, boolean checked){
		this.name = name;
		this.link = link;
		this.group = group;
		this.checked = checked;
	}
	
	public String getName(){ return this.name; }
		
	public boolean getChecked(){return this.checked; }
	
	public int getLink(){ return link; }
	
	public int getGroup(){ return group; }
		
	public ContentValues getContentValues(){

		ContentValues cv = new ContentValues();
		
		cv.put(ListTable.COLUMN_NAME, this.getName());
		cv.put(ListTable.COLUMN_CHECK, this.getChecked() ? 1 : 0);
		cv.put(ListTable.COLUMN_LINK, this.getLink() );
		cv.put(ListTable.COLUMN_GROUP, this.getLink() );
		
		return cv;
			
	}
	
	public static List<ChecklistItem> generateItems(Cursor c){
		List<ChecklistItem> items = new ArrayList<ChecklistItem>();
		
		while(c.moveToNext()){
			String name = c.getString(c.getColumnIndex(ListTable.COLUMN_NAME));
			boolean checked = c.getInt(c.getColumnIndex(ListTable.COLUMN_CHECK)) == 1;
			int link = c.getInt(c.getColumnIndex(ListTable.COLUMN_LINK));
			int group = c.getInt(c.getColumnIndex(ListTable.COLUMN_GROUP));
			
			items.add(new ChecklistItem(name, link, group, checked));
		}
		
		return items;
	}
}
