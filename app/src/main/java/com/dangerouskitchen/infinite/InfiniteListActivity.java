package com.dangerouskitchen.infinite;

/* 
Infinite Checklist - A nice and simple Android app
Copyright (C) 2014 Casey Watson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
 
import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;

public class InfiniteListActivity extends Activity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist);
             
        if (savedInstanceState == null) {
        	InfiniteListFragment fragment = InfiniteListFragment.newInstance("Master", 0);
        	FragmentTransaction trans = getFragmentManager().beginTransaction();
        	trans.add(R.id.container, fragment);
        	//trans.addToBackStack(null);
            trans.commit();
        }
    }

    public void showList(String name, int link){
    	InfiniteListFragment fragment = InfiniteListFragment.newInstance(name, link);
    	    	
    	FragmentTransaction transaction = getFragmentManager().beginTransaction();
    	transaction.replace(R.id.container, fragment);
    	transaction.addToBackStack( null );
    	transaction.commit();
    }
    
    public void setActionBarTitle(String title){
    	getActionBar().setTitle(getString(R.string.app_name) + ": " + title);
    }

    @Override
    public void onBackPressed() {
    	if (getFragmentManager().getBackStackEntryCount() > 1){
    		getFragmentManager().popBackStack();
    	} else {
    		super.onBackPressed();
    	}
    }
}
