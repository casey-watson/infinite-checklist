package com.dangerouskitchen.infinite.db;

/* 
Infinite Checklist - A nice and simple Android app
Copyright (C) 2014 Casey Watson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class InfiniteContentProvider extends ContentProvider{

	private DatabaseHandler database;
	
	private static final String AUTHORITY = "com.dangerouskitchen.infinite.contentprovider";
	private static final String BASE_PATH = "infinite_items";
	public static final Uri CONTENT_URI = Uri.parse("content://"+AUTHORITY+"/"+BASE_PATH);
	
	@Override
	public boolean onCreate() {
		database = new DatabaseHandler(getContext());
		return true;
	}

 
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
			String sortOrder) {
		
		SQLiteDatabase db = database.getWritableDatabase();
		
		Cursor cursor = db.query(DatabaseHandler.MASTER_TABLENAME, 
				projection, selection, selectionArgs, null, null, null);
		
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		
		return cursor;
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		
		SQLiteDatabase db = database.getWritableDatabase();
		
		int rowsDeleted = db.delete(DatabaseHandler.MASTER_TABLENAME, selection, selectionArgs);
		
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase db = database.getWritableDatabase();
		
		long id = db.insert(DatabaseHandler.MASTER_TABLENAME, null, values);
			
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		SQLiteDatabase db = database.getWritableDatabase();
		int rowsUpdated = db.update(DatabaseHandler.MASTER_TABLENAME, values, selection, selectionArgs);
		
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}
	
}
