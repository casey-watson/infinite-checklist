package com.dangerouskitchen.infinite.db;

/* 
Infinite Checklist - A nice and simple Android app
Copyright (C) 2014 Casey Watson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import java.util.List;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.dangerouskitchen.infinite.db.DatabaseHandler.ListTable;
import com.dangerouskitchen.infinite.items.ChecklistItem;

public class InfiniteContentResolver extends ContentResolver{
	
	private ContentResolver resolver;
	
	public InfiniteContentResolver(Context context){
		super(context);
		resolver = context.getContentResolver();
	}
	
	public void updateItemChecked(int id, boolean checked){
		ContentValues values = new ContentValues();
		values.put(ListTable.COLUMN_CHECK, checked);
	
		resolver.update(InfiniteContentProvider.CONTENT_URI,
				values, ListTable.COLUMN_ID + " = " + id, null	);

	}
	
	public void updateItemName(int id, String newName){
		ContentValues values = new ContentValues();
		values.put(ListTable.COLUMN_NAME, newName);
		
		resolver.update(InfiniteContentProvider.CONTENT_URI,
				values, ListTable.COLUMN_ID + " = " + id, null	);

	}
	
	public void addItemToList(int group, String name){
		ContentValues values = new ContentValues();
		values.put(ListTable.COLUMN_LINK, -1);
		values.put(ListTable.COLUMN_GROUP, group);
		values.put(ListTable.COLUMN_NAME, name);
		
		resolver.insert(InfiniteContentProvider.CONTENT_URI, values);
	}
	
	public void addListToList(int group, String name){
		int nextLink = getNextLinkNumber();
		ContentValues values = new ContentValues();
		values.put(ListTable.COLUMN_LINK, nextLink);
		values.put(ListTable.COLUMN_GROUP, group);
		values.put(ListTable.COLUMN_NAME, name);
		
		resolver.insert(InfiniteContentProvider.CONTENT_URI, values);
	}
	
	public void deleteCheckedItems(int group){
		deleteItems(group, false);
	}

	private void deleteItems(int group, boolean forceDelete){

		Cursor c = resolver.query(InfiniteContentProvider.CONTENT_URI, ListTable.allColumns, ListTable.COLUMN_GROUP + " = " + group, null, null);
		
		List<ChecklistItem> items = ChecklistItem.generateItems(c);
		c.close();
		
		for(ChecklistItem item : items){
			if(item.getLink() > 0){ // item is a list
				if( forceDelete || item.getChecked()){ // delete all the items from the list
					deleteItems(item.getLink(), true);
				}
			}
		}
		
		if( forceDelete ){
			resolver.delete(InfiniteContentProvider.CONTENT_URI, 
					ListTable.COLUMN_GROUP + " = ?", 
					new String[]{String.valueOf(group)});
		}else{
			resolver.delete(InfiniteContentProvider.CONTENT_URI, 
					ListTable.COLUMN_GROUP + " = ? AND " + ListTable.COLUMN_CHECK + " = ?", 
					new String[]{String.valueOf(group), "1"});			
		
		}

	}
	
	private int getNextLinkNumber(){
		int greatestLink = 0;
		Cursor c = resolver.query(InfiniteContentProvider.CONTENT_URI,
				new String[]{"MAX(" + ListTable.COLUMN_LINK + ")"},
				null, null, null
				);
		if(c.moveToFirst()){
			greatestLink = Math.max(greatestLink,c.getInt(0));
		} 
		c.close();
		
		// check for max GROUP also so we don't reuse ids of deleted lists

		c = resolver.query(InfiniteContentProvider.CONTENT_URI,
				new String[]{"MAX(" + ListTable.COLUMN_GROUP + ")"},
				null, null, null
				);
		if(c.moveToFirst()){
			greatestLink = Math.max(greatestLink, c.getInt(0));
		}
		c.close();
		
		return greatestLink + 1;
	}

}
