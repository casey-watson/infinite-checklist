package com.dangerouskitchen.infinite.db;

/* 
Infinite Checklist - A nice and simple Android app
Copyright (C) 2014 Casey Watson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper{
	
	
	
	public static final int DATABASE_VERSION = 3;
	public static final String DATABASE_NAME = "infiniteDB.db";
	
	
	public static final String MASTER_TABLENAME = "master_table";
	
	public interface DatabaseProvider{
		DatabaseHandler getDB();
	}
	
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db){
		Log.w("db", "onCreate");
		String sql = ListTable.getCreate(MASTER_TABLENAME);
		db.execSQL(sql);	
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int old_version, int new_version){
		// not going to attempt to migrate data in this version, < 10 total downloads
		Log.w("db", "onUpgrade " + old_version + " " + new_version);
		db.execSQL("DROP TABLE IF EXISTS " + MASTER_TABLENAME);
		onCreate(db);
	}

	public static class ListTable{
		public static final String COLUMN_ID = "_id";
		public static final String COLUMN_NAME = "name";
		public static final String COLUMN_CHECK = "checked";
		public static final String COLUMN_LINK = "link";
		public static final String COLUMN_GROUP = "l_group";

		public static final String[] allColumns = {COLUMN_ID, COLUMN_NAME, COLUMN_CHECK, COLUMN_LINK, COLUMN_GROUP};
		public static final String[] columnTypes = { "INTEGER PRIMARY KEY", "TEXT", "TEXT", "TEXT", "TEXT"};

		public static String getCreate(String tableName){
			StringBuilder builder = new StringBuilder();
			builder.append(  "CREATE TABLE [" + tableName + "] ( " );
			for(int i = 0; i < ListTable.allColumns.length; i++ ){
				builder.append(ListTable.allColumns[i]);
				builder.append(" ");
				builder.append(ListTable.columnTypes[i]);
				if(i != ListTable.allColumns.length - 1) // not the last field
					builder.append(" , ");
				else
					builder.append(" )");
			}

			return builder.toString();
		
		}
	}

	
}
