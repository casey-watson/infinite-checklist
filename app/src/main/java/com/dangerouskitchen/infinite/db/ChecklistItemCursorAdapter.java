package com.dangerouskitchen.infinite.db;

/* 
Infinite Checklist - A nice and simple Android app
Copyright (C) 2014 Casey Watson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dangerouskitchen.infinite.R;
import com.dangerouskitchen.infinite.db.DatabaseHandler.ListTable;

public class ChecklistItemCursorAdapter extends CursorAdapter{

	private InfiniteContentResolver mContentResolver;
	private Context mContext;
	
	public ChecklistItemCursorAdapter(Context context,Cursor c){
		super(context, c, false);
		mContext = context;
		mContentResolver = new InfiniteContentResolver(mContext);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {

		LayoutInflater inflater = LayoutInflater.from(context);
		View view =  inflater.inflate(R.layout.list_item_row, parent, false);

		boolean checked = cursor.getInt(cursor.getColumnIndex(ListTable.COLUMN_CHECK)) == 1;
		
		ListItemHolder holder = new ListItemHolder();
		holder.chkBox = (CheckBox) view.findViewById(R.id.chkBox);
		holder.chkBox.setChecked(checked);
		holder.tvName = (TextView) view.findViewById(R.id.txvName);
		holder.imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
		view.setTag(holder);
		
		return view;
	}
	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		
		final int id = cursor.getInt(cursor.getColumnIndex(ListTable.COLUMN_ID));
		String name = cursor.getString(cursor.getColumnIndex(ListTable.COLUMN_NAME));
		int checkedint = cursor.getInt(cursor.getColumnIndex(ListTable.COLUMN_CHECK));
        boolean icon_visible = cursor.getInt( cursor.getColumnIndex(ListTable.COLUMN_LINK)) > 0;
             
        boolean checked = checkedint == 0 ? false : true; 
        
		ListItemHolder holder = (ListItemHolder) view.getTag();
		
        holder.tvName.setText(name );
        holder.tvName.setOnClickListener(new UpdateNameListener(mContext, id));
        
        holder.chkBox.setOnCheckedChangeListener(null);
        holder.chkBox.setChecked(checked);
        holder.chkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mContentResolver.updateItemChecked(id, isChecked);
			}
		});
        
        holder.imgIcon.setVisibility( icon_visible ? View.VISIBLE : View.INVISIBLE);
	}

	public class UpdateNameListener implements OnClickListener
	{
		int id;
		Context context;
		public UpdateNameListener(Context context, int id){ 
			this.id = id;
			this.context = context;
		}

		@Override
		public void onClick(View v) {
			String text = ((TextView)v).getText().toString();
			final EditText input = new EditText(context);
			input.setText( ((TextView)v).getText() );
			input.setSelection(text.length());
			AlertDialog dialog = new AlertDialog.Builder(context)
		    .setTitle(context.getString(R.string.edit_name_title))
		    .setView(input)
		    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		        	String value = input.getText().toString();
		        	mContentResolver.updateItemName(id, value);

		        }
		    })
		    .setNegativeButton(context.getString(android.R.string.cancel), null)
		    .create();
			dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		    dialog.show();
		}
	}
	
	private static class ListItemHolder{
		CheckBox chkBox;
		TextView tvName;
		ImageView imgIcon;
	}
	
}
