package com.dangerouskitchen.infinite;

/* 
Infinite Checklist - A nice and simple Android app
Copyright (C) 2014 Casey Watson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.dangerouskitchen.infinite.db.ChecklistItemCursorAdapter;
import com.dangerouskitchen.infinite.db.DatabaseHandler.ListTable;
import com.dangerouskitchen.infinite.db.InfiniteContentProvider;
import com.dangerouskitchen.infinite.db.InfiniteContentResolver;

public class InfiniteListFragment extends Fragment implements LoaderCallbacks<Cursor>
{
	private static final String KEY_GROUP = "group";
	private static final String KEY_NAME = "name";
	
	int mGroup;
	String mName;

	InfiniteContentResolver mResolver;
	ChecklistItemCursorAdapter mAdapter;
	
	/** UI ELEMENTS **/
	ListView listView;
	//TextView tvListTitle;
	/** UI **/
	
	public static InfiniteListFragment newInstance(String name, int link){
		InfiniteListFragment frag = new InfiniteListFragment();
		
		Bundle args = new Bundle();
		args.putString(KEY_NAME, name);
		args.putInt(KEY_GROUP, link);
		
		frag.setArguments(args);
		
		return frag;
	}
	
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		CursorLoader loader = new CursorLoader(getActivity(), 
				InfiniteContentProvider.CONTENT_URI,
				ListTable.allColumns,
				ListTable.COLUMN_GROUP + " = ?",
				new String[]{ String.valueOf(id) },
				null
				);	
		
		return loader;
	}
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mAdapter.swapCursor(cursor);
		Log.d("cursor", "onLoadFinished");
	}
	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		mAdapter.swapCursor(null);
		
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
    	setHasOptionsMenu(true);
    	
    	mName = getArguments().getString(KEY_NAME);
    	mGroup = getArguments().getInt(KEY_GROUP);
    	
    	getLoaderManager().initLoader(mGroup, null, this);
    	
        View view = inflater.inflate(R.layout.fragment_checklist, container, false);
        
        listView = (ListView) view.findViewById(R.id.masterListView);
        listView.setOnItemClickListener(listClickListener);
  
        TextView emptyTextView = (TextView) view.findViewById(R.id.empty);
        listView.setEmptyView(emptyTextView);
        
    	//tvListTitle = (TextView) view.findViewById(R.id.tv_list_name);
        if(mGroup > 0){
        	getActivity().setTitle(mName);
        	//tvListTitle.setText(mName);
        } else { 
        	getActivity().setTitle(R.string.app_name);
        	//tvListTitle.setVisibility(View.GONE);
        }

        mAdapter = new ChecklistItemCursorAdapter(getActivity(),null);

        mResolver = new InfiniteContentResolver(getActivity());
        
        listView.setAdapter(mAdapter);
        
        return view;
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
    	inflater.inflate(R.menu.menu_list,menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
    	// handle item selection
    	switch (item.getItemId()) {
    	case R.id.action_delete_selected:
    		deleteChecked();
    		return true;
    	case R.id.action_add_item:
    		addItem();
    		return true;
    	case R.id.action_add_list:
    		addList();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
	
    private OnItemClickListener listClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Cursor c = (Cursor) parent.getAdapter().getItem(position);
			int link = c.getInt(c.getColumnIndex(ListTable.COLUMN_LINK));
			String name = c.getString(c.getColumnIndex(ListTable.COLUMN_NAME));
			if(link > 0){
				((InfiniteListActivity)getActivity()).showList(name,link);
			}
		}
	};
    
	private void addItem(){
		final EditText input = new EditText(getActivity());
		AlertDialog dialog = new AlertDialog.Builder(getActivity())
		.setTitle(getString(R.string.enter_item_name_title))
		.setView(input)
		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				mResolver.addItemToList(mGroup, value);
			}
		})
		.setNegativeButton(getString(android.R.string.cancel), null)
		.create();
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		dialog.show();
	}

	private void addList(){
		final EditText input = new EditText(getActivity());
		AlertDialog dialog = new AlertDialog.Builder(getActivity())
		.setTitle(getString(R.string.enter_list_name_title))
		.setView(input)
		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				mResolver.addListToList(mGroup, value);
			}
		})
		.setNegativeButton(android.R.string.cancel, null)
		.create();
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		dialog.show();
	}
	
	private void deleteChecked(){
		mResolver.deleteCheckedItems(mGroup);
	}


}
